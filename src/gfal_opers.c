/*
 * Copyright (c) CERN 2013-2017
 *
 * Copyright (c) Members of the EMI Collaboration. 2010-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** @file gfal_opers.c
 * Map of the gfal operators
 **/

#define _GNU_SOURCE

#include <string.h>
#include <errno.h>
#include <math.h>
#include <gfal_api.h>
#include "gfal_opers.h"


char mount_point[2048];
size_t s_mount_point = 0;

char local_mount_point[2048];
size_t s_local_mount_point = 0;

gboolean guid_mode = FALSE;


/**
 * Logs the error, frees it, and return the -code
 */
static int gfalfs_handle_error(const char *func, GError *error)
{
    int code = error->code;
    if (code != ENOATTR) {
        gfal2_log(G_LOG_LEVEL_WARNING, "%s: [%s] %s (%d)", func,
            g_quark_to_string(error->domain), error->message, error->code);
    }
    g_error_free(error);
    return -code;
}


static void gfalfs_tune_stat(struct stat *st)
{
    // tune block size to 16Mega for cp optimization with big files on network file system
    st->st_blksize = (1 << 24);
    // workaround for utilities like du that use st_blocks (LCGUTIL-289)
    st->st_blocks = ceil(st->st_size / 512.0);
}


void gfalfs_set_local_mount_point(const char *local_mp)
{
    g_strlcpy(local_mount_point, local_mp, 2048);
    s_local_mount_point = strlen(local_mount_point);
}


void gfalfs_set_remote_mount_point(const char *remote_mp)
{
    g_strlcpy(mount_point, remote_mp, 2048);
    s_mount_point = strlen(remote_mp);
}


void gfalfs_construct_path(const char *path, char *buff, size_t s_buff)
{
    if (guid_mode) {
        g_strlcpy(buff, path + 1, s_buff);
    }
    else {
        char *p = (char *) mempcpy(buff, mount_point, MIN(s_buff - 1, s_mount_point));
        p = mempcpy(p, path, MIN(s_buff - 1 - (p - buff), strlen(path)));
        *p = '\0';
    }
}


void gfalfs_construct_path_from_abs_local(const char *path, char *buff, size_t s_buff)
{
    char tmp_buff[2048];
    if (strstr(path, local_mount_point) == (char *) path) {
        g_strlcpy(tmp_buff, path + s_local_mount_point, 2048);
    }
    gfalfs_construct_path(tmp_buff, buff, s_buff);
}

// convert a remote path of result in a local path
static void
convert_external_readlink_to_local_readlink(char *external_buff, size_t s_ext, char *local_buff, ssize_t s_local)
{
    if (s_local > 0) {
        char internal_buff[2048];
        if (s_ext > s_mount_point) {
            size_t s_input = MIN(s_ext, 2048 - 1);
            *((char *) mempcpy(internal_buff, external_buff + s_mount_point, s_input - s_mount_point)) = '\0';

            g_strlcpy(local_buff, local_mount_point, s_local);
            if (s_local > s_local_mount_point) {
                g_strlcat(local_buff, internal_buff, s_local);
            }
        }
        else {
            *((char *) mempcpy(local_buff, external_buff, MIN(s_ext, s_local - 1))) = '\0';
        }

    }
}


static int gfalfs_getattr(const char *path, struct stat *stbuf)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_getattr path %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    if(gfal2_lstat(ctx, full_url, stbuf, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    gfalfs_tune_stat(stbuf);
    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_readlink(const char *path, char *link_buff, size_t buffsiz)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_readlink path %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    char tmp_link_buff[2048];
    GError *error = NULL;
    ssize_t ret = gfal2_readlink(ctx, full_url, tmp_link_buff, sizeof(tmp_link_buff), &error);
    if (ret < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    convert_external_readlink_to_local_readlink(tmp_link_buff, ret, link_buff, buffsiz);

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_opendir(const char *path, struct fuse_file_info *f)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_opendir path %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    DIR *i = gfal2_opendir(ctx, full_url, &error);
    if (i == NULL) {
        return gfalfs_handle_error(__func__, error);
    }

    f->fh = (uint64_t)i;
    return 0;
}


static int gfalfs_releasedir(const char *path, struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_closedir fd : %d", fi->fh);

    DIR *d = (DIR*)fi->fh;
    GError *error = NULL;
    if (gfal2_closedir(ctx, d, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    return 0;
}


static int gfalfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
    off_t offset, struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_readdir path %s", path);

    DIR *d = (DIR*)fi->fh;
    GError *error = NULL;

    struct stat st;
    struct dirent *dent;

    while ((dent = gfal2_readdirpp(ctx, d, &st, &error)) != NULL) {
        gfalfs_tune_stat(&st);

        if (filler(buf, dent->d_name, &st, offset++) != 0) {
            gfal2_log(G_LOG_LEVEL_DEBUG, "Filler full");
            break;
        }

        if (fuse_interrupted()) {
            return -ECANCELED;
        }
    }

    if (error) {
        return gfalfs_handle_error(__func__, error);
    }

    return 0;
}


static int gfalfs_open(const char *path, struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_open path %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    int fd = gfal2_open2(ctx, full_url, fi->flags, 755, &error);
    if (fd < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    fi->fh = fd;
    return 0;
}


static int gfalfs_creat(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_creat path %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, 2048);

    GError *error = NULL;
    int fd = gfal2_creat(ctx, full_url, mode, &error);
    if (fd < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    fi->fh = fd;
    return 0;
}


static int gfalfs_release(const char *path, struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_close fd : %d", fi->fh);

    GError *error = NULL;
    if (gfal2_close(ctx, fi->fh, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    return 0;
}


static int gfalfs_read(const char *path, char *buf, size_t size, off_t offset,
    struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_read path : %s fd : %d", path, fi->fh);

    GError *error = NULL;
    ssize_t ret = gfal2_pread(ctx, fi->fh, buf, size, offset, &error);
    if (ret < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return ret;
}


static int gfalfs_write(const char *path, const char *buf, size_t size, off_t offset,
    struct fuse_file_info *fi)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_write path : %s fd : %d", path, fi->fh);

    GError *error = NULL;
    ssize_t ret = gfal2_pwrite(ctx, fi->fh, (const void *) buf, size, offset, &error);
    if (ret < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return ret;
}


int gfalfs_chown(const char *path, uid_t uid, gid_t guid)
{
    // do nothing, change prop not authorized
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_chown path : %s", path);
    return 0;
}


int gfalfs_utimens(const char *path, const struct timespec tv[2])
{
    // do nothing, not implemented yet
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_utimens path : %s", path);
    return 0;
}


int gfalfs_truncate(const char *path, off_t size)
{
    // do nothing, not implemented yet
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_truncate path : %s", path);
    return 0;
}


static int gfalfs_access(const char *path, int flag)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_access path : %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    if (gfal2_access(ctx, full_url, flag, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_unlink(const char *path)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_access path : %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    if (gfal2_unlink(ctx, full_url, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_mkdir(const char *path, mode_t mode)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_mkdir path : %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    if (gfal2_mkdir(ctx, full_url, mode, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_getxattr(const char *path, const char *name, char *buff, size_t s_buff)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_getxattr path : %s, name : %s, size %d", path, name, s_buff);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    int ret = gfal2_getxattr(ctx, full_url, name, buff, s_buff, &error);
    if (ret < 0) {
        if (error->code == EPROTONOSUPPORT) { // silent the non supported errors
            error->code = ENOATTR;
        }
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return ret;
}


static int gfalfs_setxattr(const char *path, const char *name, const char *buff, size_t s_buff, int flag)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_setxattr path : %s, name : %s", path, name);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    if (gfal2_setxattr(ctx, full_url, name, buff, s_buff, flag, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_listxattr(const char *path, char *list, size_t s_list)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_listxattr path : %s, size %d", path, s_list);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, sizeof(full_url));

    GError *error = NULL;
    int ret = gfal2_listxattr(ctx, full_url, list, s_list, &error);
    if (ret < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return ret;
}


static int gfalfs_rename(const char *oldpath, const char *newpath)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_rename oldpath : %s, newpath : %s", oldpath, newpath);

    char full_url_old[2048];
    char full_url_new[2048];
    gfalfs_construct_path(oldpath, full_url_old, sizeof(full_url_old));
    gfalfs_construct_path(newpath, full_url_new, sizeof(full_url_new));

    GError *error = NULL;
    if (gfal2_rename(ctx, full_url_old, full_url_new, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_symlink(const char *oldpath, const char *newpath)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_symlink oldpath : %s, newpath : %s", oldpath, newpath);

    char full_url_old[2048];
    char full_url_new[2048];
    gfalfs_construct_path_from_abs_local(oldpath, full_url_old, sizeof(full_url_old));
    gfalfs_construct_path(newpath, full_url_new, sizeof(full_url_new));

    GError *error = NULL;
    if (gfal2_symlink(ctx, full_url_old, full_url_new, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


static int gfalfs_chmod(const char *path, mode_t mode)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_chmod path : %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, 2048);

    GError *error = NULL;
    if (gfal2_chmod(ctx, full_url, mode, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;

}


static int gfalfs_rmdir(const char *path)
{
    gfal2_context_t ctx = (gfal2_context_t)fuse_get_context()->private_data;
    gfal2_log(G_LOG_LEVEL_MESSAGE, "gfalfs_rmdir path : %s", path);

    char full_url[2048];
    gfalfs_construct_path(path, full_url, 2048);

    GError *error = NULL;
    if (gfal2_rmdir(ctx, full_url, &error) < 0) {
        return gfalfs_handle_error(__func__, error);
    }

    if (fuse_interrupted()) {
        return -ECANCELED;
    }
    return 0;
}


int gfalfs_fake_fgetattr(const char *url, struct stat *st, struct fuse_file_info *f)
{
    // On file creation, do not bother because it is possible that it doesn't even exist yet
    // (i.e. http), or we want to avoid the latency penalty
    if (f->flags & O_CREAT) {
        gfal2_log(G_LOG_LEVEL_MESSAGE, "fgetattr create mode, bypass and set to default, speed hack");
        memset(st, 0, sizeof(struct stat));
        st->st_mode = S_IFREG | 0666;
        return 0;
    }
    else {
        gfal2_log(G_LOG_LEVEL_MESSAGE, "fgetattr other mode");
        return gfalfs_getattr(url, st);
    }
}


void *gfalfs_init(struct fuse_conn_info *conn)
{
    gfal2_log(G_LOG_LEVEL_INFO, "Creating gfal2 context");

    GError *error = NULL;
    gfal2_context_t ctx = gfal2_context_new(&error);
    if (!ctx) {
        gfal2_log(G_LOG_LEVEL_CRITICAL, "Failed to instantiate the gfal2 context: %s", error->message);
        g_clear_error(&error);
        return NULL;
    }

    char version_str[32];
    snprintf(version_str, sizeof(version_str), "%d.%d", conn->proto_major, conn->proto_minor);
    if (gfal2_set_user_agent(ctx, "gfalFS", version_str, &error) != 0) {
        gfal2_log(G_LOG_LEVEL_ERROR, "Failed to set the user agent: %s", error->message);
        g_clear_error(&error);
    }

    return ctx;
}


void gfalfs_destroy(void *ptr)
{
    gfal2_log(G_LOG_LEVEL_INFO, "Destroying gfal2 context");
    gfal2_context_t ctx = (gfal2_context_t)ptr;
    gfal2_context_free(ctx);
}


struct fuse_operations gfal_oper = {
    .getattr    = gfalfs_getattr,
    .readdir    = gfalfs_readdir,
    .opendir    = gfalfs_opendir,
    .open       = gfalfs_open,
    .read       = gfalfs_read,
    .release    = gfalfs_release,
    .releasedir = gfalfs_releasedir,
    .access     = gfalfs_access,
    .create     = gfalfs_creat,
    .mkdir      = gfalfs_mkdir,
    .rmdir      = gfalfs_rmdir,
    .chmod      = gfalfs_chmod,
    .rename     = gfalfs_rename,
    .write      = gfalfs_write,
    .chown      = gfalfs_chown,
    .utimens    = gfalfs_utimens,
    .truncate   = gfalfs_truncate,
    .symlink    = gfalfs_symlink,
    .setxattr   = gfalfs_setxattr,
    .getxattr   = gfalfs_getxattr,
    .fgetattr   = gfalfs_fake_fgetattr,
    .listxattr  = gfalfs_listxattr,
    .readlink   = gfalfs_readlink,
    .unlink     = gfalfs_unlink,
    .init       = gfalfs_init,
    .destroy    = gfalfs_destroy
};
